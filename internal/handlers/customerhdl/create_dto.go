package customerhdl

import "gitlab.com/phil-go/customer-service/internal/core/domain"

type QueryCustomerDTO struct {
	ID      string `json:"id"`
	Name    string `json:"name"`
	Email   string `json:"email"`
	Address struct {
		Street     string `json:"street"`
		Number     string `json:"number"`
		Complement string `json:"complement"`
		ZIP        string `json:"zip"`
		City       string `json:"city"`
		State      string `json:"state"`
		Country    string `json:"country"`
	}
}

type CreateCustomerDTO struct {
	Name    string `json:"name"`
	Email   string `json:"email"`
	Address struct {
		Street     string `json:"street"`
		Number     string `json:"number"`
		Complement string `json:"complement"`
		ZIP        string `json:"zip"`
		City       string `json:"city"`
		State      string `json:"state"`
		Country    string `json:"country"`
	}
}

func ToDomain(data CreateCustomerDTO) domain.Customer {
	result := new(domain.Customer)
	result.Name = data.Name
	result.Email = data.Email
	result.Name = data.Name
	result.Email = data.Email
	result.Address.Street = data.Address.Street
	result.Address.Number = data.Address.Number
	result.Address.Complement = data.Address.Complement
	result.Address.ZIP = data.Address.ZIP
	result.Address.City = data.Address.City
	result.Address.State = data.Address.State
	result.Address.Country = data.Address.Country

	return *result
}

func FromDomain(data domain.Customer) QueryCustomerDTO {

	result := new(QueryCustomerDTO)
	result.ID = data.ID
	result.Name = data.Name
	result.Email = data.Email
	result.Address.Street = data.Address.Street
	result.Address.Number = data.Address.Number
	result.Address.Complement = data.Address.Complement
	result.Address.ZIP = data.Address.ZIP
	result.Address.City = data.Address.City
	result.Address.State = data.Address.State
	result.Address.Country = data.Address.Country

	return *result
}
