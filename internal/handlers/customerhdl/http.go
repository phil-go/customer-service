package customerhdl

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/phil-go/customer-service/internal/core/ports"
)

type HTTPHandler struct {
	service ports.CustomerService
}

func NewHTTPHandler(service ports.CustomerService) *HTTPHandler {
	return &HTTPHandler{
		service: service,
	}
}

func (hdl *HTTPHandler) Create(c *gin.Context) {
	body := CreateCustomerDTO{}
	c.BindJSON(&body)

	customer, err := hdl.service.Create(ToDomain(body))

	if err != nil {
		c.AbortWithStatusJSON(500, gin.H{"message": err.Error()})
		return
	}

	c.JSON(200, FromDomain(customer))

}

func (hdl *HTTPHandler) Find(c *gin.Context) {
	customer, err := hdl.service.Find(c.Param("id"))
	if err != nil {
		c.AbortWithStatusJSON(500, gin.H{"message": err.Error()})
		return
	}

	c.JSON(200, FromDomain(customer))
}
