package customerrepo

import (
	"encoding/json"

	"github.com/matiasvarela/errors"

	"gitlab.com/phil-go/customer-service/internal/core/domain"
	"gitlab.com/phil-go/customer-service/pkg/apperrors"
)

type memkvs struct {
	kvs map[string][]byte
}

func NewMemKVS() *memkvs {
	return &memkvs{kvs: map[string][]byte{}}
}

func (repo *memkvs) Find(id string) (domain.Customer, error) {
	if value, ok := repo.kvs[id]; ok {
		customer := domain.Customer{}
		err := json.Unmarshal(value, &customer)
		if err != nil {
			return domain.Customer{}, errors.New(apperrors.Internal, err, "fail to get value from kvs")
		}

		return customer, nil
	}

	return domain.Customer{}, errors.New(apperrors.NotFound, nil, "customer not found in kvs")
}

func (repo *memkvs) Create(customer domain.Customer) error {
	bytes, err := json.Marshal(customer)
	if err != nil {
		return errors.New(apperrors.InvalidInput, err, "customer fails at marshal into json string")
	}

	repo.kvs[customer.ID] = bytes

	return nil
}
