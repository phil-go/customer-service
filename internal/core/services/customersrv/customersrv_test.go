package customersrv_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/golang/mock/gomock"

	"github.com/matiasvarela/errors"
	"gitlab.com/phil-go/customer-service/internal/core/domain"
	"gitlab.com/phil-go/customer-service/internal/core/services/customersrv"
	"gitlab.com/phil-go/customer-service/mocks/mockups"
	"gitlab.com/phil-go/customer-service/pkg/apperrors"
)

type mocks struct {
	repository *mockups.MockCustomersRepository
	uidGen     *mockups.MockUIDGen
}

func TestFindCustomer(t *testing.T) {
	customer := easymockCustomer("1001-1001-1001-1001", "Philippe Tavares", "phil.estudos@gmail.com", "Rua Otton da Fonseca", "40", "BL 3 AP 405", "21741230", "Rio de Janeiro", "RJ", "Brasil")

	type args struct {
		id string
	}

	type want struct {
		result domain.Customer
		err    error
	}

	tests := []struct {
		name  string
		args  args
		want  want
		mocks func(m mocks)
	}{
		{
			name: "Should get customer successfully",
			args: args{id: "1001-1001-1001-1001"},
			want: want{result: customer},
			mocks: func(m mocks) {
				m.repository.EXPECT().Find("1001-1001-1001-1001").Return(customer, nil)
			},
		}, {
			name: "Should return error - customer not found",
			args: args{id: "1001-1001-1001-1001"},
			want: want{
				result: domain.Customer{},
				err:    errors.New(apperrors.NotFound, nil, "customer not found"),
			},
			mocks: func(m mocks) {
				m.repository.EXPECT().Find("1001-1001-1001-1001").Return(domain.Customer{}, errors.New(apperrors.NotFound, nil, ""))
			},
		}, {
			name: "Should return error - find customer failed",
			args: args{id: "1001-1001-1001-1001"},
			want: want{
				result: domain.Customer{},
				err:    errors.New(apperrors.Internal, nil, "find customer from repository has failed"),
			},
			mocks: func(m mocks) {
				m.repository.EXPECT().Find("1001-1001-1001-1001").Return(domain.Customer{}, errors.New(apperrors.Internal, nil, ""))
			},
		},
	}

	for _, tt := range tests {
		tt := tt

		// Prepare
		m := mocks{
			repository: mockups.NewMockCustomersRepository(gomock.NewController(t)),
			uidGen:     mockups.NewMockUIDGen(gomock.NewController(t)),
		}

		tt.mocks(m)

		service := customersrv.New(m.repository, m.uidGen)

		result, err := service.Find(tt.args.id)

		// Verify
		if tt.want.err != nil && err != nil {
			assert.Equal(t, errors.Code(tt.want.err), errors.Code(err))
			assert.Equal(t, tt.want.err.Error(), err.Error())
		}

		assert.Equal(t, tt.want.result, result)
	}

}

func TestCreateCustomer(t *testing.T) {
	mockCustomer := easymockCustomer("1001-1001-1001-1001", "Philippe Tavares", "phil.estudos@gmail.com", "Rua Otton da Fonseca", "40", "BL 3 AP 405", "21741230", "Rio de Janeiro", "RJ", "Brasil")

	address := domain.NewAddress("Rua Otton da Fonseca", "40", "BL 3 AP 405", "21741230", "Rio de Janeiro", "RJ", "Brasil")
	customer := domain.NewCustomer("", "Philippe Tavares", "phil.estudos@gmail.com", address)

	type args struct {
		customer domain.Customer
	}

	type want struct {
		result domain.Customer
		err    error
	}

	tests := []struct {
		name  string
		args  args
		want  want
		mocks func(m mocks)
	}{
		{
			name: "Should create a new customer successfully",
			args: args{customer: customer},
			want: want{result: mockCustomer},
			mocks: func(m mocks) {
				m.uidGen.EXPECT().New().Return("1001-1001-1001-1001")
				m.repository.EXPECT().Create(gomock.Any()).Return(nil)
			},
		}, {
			name: "Should return an error - create customer into repository fails",
			args: args{customer: customer},
			want: want{err: errors.New(apperrors.Internal, nil, "create customer into repository has failed")},
			mocks: func(m mocks) {
				m.uidGen.EXPECT().New().Return("1001-1001-1001-1001")
				m.repository.EXPECT().Create(gomock.Any()).Return(errors.New(apperrors.Internal, nil, ""))
			},
		},
	}

	for _, tt := range tests {
		tt := tt

		// Prepare
		m := mocks{
			repository: mockups.NewMockCustomersRepository(gomock.NewController(t)),
			uidGen:     mockups.NewMockUIDGen(gomock.NewController(t)),
		}

		tt.mocks(m)
		service := customersrv.New(m.repository, m.uidGen)

		// Execute
		result, err := service.Create(tt.args.customer)

		// Verify
		if tt.want.err != nil && err != nil {
			assert.Equal(t, errors.Code(tt.want.err), errors.Code(err))
			assert.Equal(t, tt.want.err.Error(), err.Error())
		}

		assert.Equal(t, tt.want.result.ID, result.ID)
		assert.Equal(t, tt.want.result.Name, result.Name)
		assert.Equal(t, tt.want.result.Email, result.Email)
		assert.Equal(t, tt.want.result.Address.Street, result.Address.Street)
		assert.Equal(t, tt.want.result.Address.Number, result.Address.Number)
		assert.Equal(t, tt.want.result.Address.Complement, result.Address.Complement)
		assert.Equal(t, tt.want.result.Address.ZIP, result.Address.ZIP)
		assert.Equal(t, tt.want.result.Address.City, result.Address.City)
		assert.Equal(t, tt.want.result.Address.State, result.Address.State)
		assert.Equal(t, tt.want.result.Address.Country, result.Address.Country)
	}

}

func easymockCustomer(id string, name string, email string, street string, number string, complement string, zip string, city string, state string, country string) domain.Customer {
	address := domain.NewAddress(street, number, complement, zip, city, state, country)
	customer := domain.NewCustomer(id, name, email, address)
	return customer

}
