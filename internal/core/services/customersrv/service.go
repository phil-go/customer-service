package customersrv

import (
	"github.com/matiasvarela/errors"
	"gitlab.com/phil-go/customer-service/internal/core/domain"
	"gitlab.com/phil-go/customer-service/internal/core/ports"
	"gitlab.com/phil-go/customer-service/pkg/apperrors"
	"gitlab.com/phil-go/customer-service/pkg/uidgen"
)

type service struct {
	repository ports.CustomerRepository
	uidGen     uidgen.UIDGen
}

func New(repository ports.CustomerRepository, uidGen uidgen.UIDGen) *service {
	return &service{
		repository: repository,
		uidGen:     uidGen,
	}
}

func (srv *service) Find(id string) (domain.Customer, error) {
	customer, err := srv.repository.Find(id)
	if err != nil {
		if errors.Is(err, apperrors.NotFound) {
			return domain.Customer{}, errors.New(apperrors.NotFound, err, "customer not found")
		}
		return domain.Customer{}, errors.New(apperrors.Internal, err, "find customer from repository has failed")
	}

	return customer, nil
}

func (srv *service) Create(customer domain.Customer) (domain.Customer, error) {
	uuid := srv.uidGen.New()
	customer.ID = uuid

	if err := srv.repository.Create(customer); err != nil {
		return domain.Customer{}, errors.New(apperrors.Internal, err, "create customer into repository has failed")
	}

	return customer, nil
}
