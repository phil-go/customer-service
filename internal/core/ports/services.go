package ports

import "gitlab.com/phil-go/customer-service/internal/core/domain"

type CustomerService interface {
	Find(id string) (domain.Customer, error)
	Create(customer domain.Customer) (domain.Customer, error)
}
