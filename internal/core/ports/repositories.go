package ports

import "gitlab.com/phil-go/customer-service/internal/core/domain"

type CustomerRepository interface {
	Find(id string) (domain.Customer, error)
	Create(domain.Customer) error
}
