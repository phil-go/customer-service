package domain

type Customer struct {
	ID      string  `json:"id"`
	Name    string  `json:"name"`
	Email   string  `json:"email"`
	Address Address `json:"address"`
}

func NewCustomer(id string, name string, email string, address Address) Customer {
	return Customer{
		ID:      id,
		Name:    name,
		Email:   email,
		Address: address,
	}
}
