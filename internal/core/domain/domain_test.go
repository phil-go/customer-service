package domain_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/phil-go/customer-service/internal/core/domain"
)

func TestNewAddress(t *testing.T) {
	address := domain.NewAddress("Rua Otton da Fonseca", "40", "BL 3 AP 405", "21741230", "Rio de Janeiro", "RJ", "Brasil")

	assert.Equal(t, address.Street, "Rua Otton da Fonseca")
	assert.Equal(t, address.Number, "40")
	assert.Equal(t, address.Complement, "BL 3 AP 405")
	assert.Equal(t, address.ZIP, "21741230")
	assert.Equal(t, address.City, "Rio de Janeiro")
	assert.Equal(t, address.State, "RJ")
	assert.Equal(t, address.Country, "Brasil")
}

func TestNewCustomer(t *testing.T) {
	address := domain.NewAddress("Rua Otton da Fonseca", "40", "BL 3 AP 405", "21741230", "Rio de Janeiro", "RJ", "Brasil")
	customer := domain.NewCustomer("1", "Philippe Tavares", "phil.estudos@gmail.com", address)

	assert.Equal(t, customer.ID, "1")
	assert.Equal(t, customer.Name, "Philippe Tavares")
	assert.Equal(t, customer.Email, "phil.estudos@gmail.com")
	assert.Equal(t, customer.Address.Street, "Rua Otton da Fonseca")
	assert.Equal(t, customer.Address.Number, "40")
	assert.Equal(t, customer.Address.Complement, "BL 3 AP 405")
	assert.Equal(t, customer.Address.ZIP, "21741230")
	assert.Equal(t, customer.Address.City, "Rio de Janeiro")
	assert.Equal(t, customer.Address.State, "RJ")
	assert.Equal(t, customer.Address.Country, "Brasil")
}
