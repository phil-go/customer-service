package domain

type Address struct {
	Street     string `json:"street"`
	Number     string `json:"number"`
	Complement string `json:"complement"`
	ZIP        string `json:"zip"`
	City       string `json:"city"`
	State      string `json:"state"`
	Country    string `json:"country"`
}

func NewAddress(street string, number string, complement string, zip string, city string, state string, country string) Address {
	return Address{
		Street:     street,
		Number:     number,
		Complement: complement,
		ZIP:        zip,
		City:       city,
		State:      state,
		Country:    country,
	}
}
