package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/phil-go/customer-service/internal/core/services/customersrv"
	"gitlab.com/phil-go/customer-service/internal/handlers/customerhdl"
	"gitlab.com/phil-go/customer-service/internal/repositories/customerrepo"
	"gitlab.com/phil-go/customer-service/pkg/uidgen"
)

func main() {

	repository := customerrepo.NewMemKVS()
	service := customersrv.New(repository, uidgen.New())
	handler := customerhdl.NewHTTPHandler(service)

	router := gin.New()
	router.GET("/customers/:id", handler.Find)
	router.POST("/customers", handler.Create)

	router.Run(":8080")
}
