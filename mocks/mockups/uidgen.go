package mockups

import (
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

type MockUIDGen struct {
	ctrl     *gomock.Controller
	recorder *MockUIDGenMockRecorder
}

type MockUIDGenMockRecorder struct {
	mock *MockUIDGen
}

func NewMockUIDGen(ctrl *gomock.Controller) *MockUIDGen {
	mock := &MockUIDGen{ctrl: ctrl}
	mock.recorder = &MockUIDGenMockRecorder{mock}
	return mock
}

func (m *MockUIDGen) EXPECT() *MockUIDGenMockRecorder {
	return m.recorder
}

func (m *MockUIDGen) New() string {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "New")
	ret0, _ := ret[0].(string)
	return ret0
}

func (mr *MockUIDGenMockRecorder) New() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "New", reflect.TypeOf((*MockUIDGen)(nil).New))
}
