package mockups

import (
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
	domain "gitlab.com/phil-go/customer-service/internal/core/domain"
)

type MockCustomersRepository struct {
	ctrl     *gomock.Controller
	recorder *MockCustomersRepositoryMockRecorder
}

type MockCustomersRepositoryMockRecorder struct {
	mock *MockCustomersRepository
}

func NewMockCustomersRepository(ctrl *gomock.Controller) *MockCustomersRepository {
	mock := &MockCustomersRepository{ctrl: ctrl}
	mock.recorder = &MockCustomersRepositoryMockRecorder{mock}
	return mock
}

func (m *MockCustomersRepository) EXPECT() *MockCustomersRepositoryMockRecorder {
	return m.recorder
}

func (m *MockCustomersRepository) Find(id string) (domain.Customer, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Find", id)
	ret0, _ := ret[0].(domain.Customer)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

func (mr *MockCustomersRepositoryMockRecorder) Find(id interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Find", reflect.TypeOf((*MockCustomersRepository)(nil).Find), id)
}

func (m *MockCustomersRepository) Create(arg0 domain.Customer) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Create", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

func (mr *MockCustomersRepositoryMockRecorder) Create(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Create", reflect.TypeOf((*MockCustomersRepository)(nil).Create), arg0)
}
