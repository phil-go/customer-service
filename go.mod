module gitlab.com/phil-go/customer-service

go 1.13

require (
	github.com/gin-gonic/gin v1.5.0
	github.com/golang/mock v1.4.1
	github.com/google/uuid v1.1.1
	github.com/matiasvarela/errors v0.0.0-20200210180412-00b8077e6b90
	github.com/stretchr/testify v1.5.1
)
